﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointGenerator : MonoBehaviour
{
    public GameObject pointPrefab;
    float span = 0.5f;
    float detal = 0;

    // Update is called once per frame
    void Update()
    {
        this.detal += Time.deltaTime;
        if (this.detal > this.span)
        {
            this.detal = 0;
            GameObject go = Instantiate(pointPrefab);
            int py = Random.Range(-3, 5);
            go.transform.position = new Vector3(8, py, 0);

        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    SpriteRenderer sr;
    public Sprite normal;
    public Sprite point;
    public Sprite damage;
    public Sprite health;

    void Start()
    {
        sr = GetComponent<SpriteRenderer>();
    }

    public void NormalAction()
    {
        sr.sprite = normal;
    }

    public void PointAction()
    {
        sr.sprite = point;
        Invoke("NormalAction",0.3f);   //0.3秒後にノーマルに戻る
    }

    public void DamageAction()
    {
        sr.sprite = damage;
        Invoke("NormalAction", 0.3f);　　//0.3秒後にノーマルに戻る
    }

    public void HealthAction()
    {
        sr.sprite = health;
        Invoke("NormalAction", 0.3f);  //0.3秒後にノーマルに戻る
    }
        void Update()
    {

        if (Input.GetKey(KeyCode.UpArrow))　   
        {
            transform.Translate(0, 0.05f, 0);　
        }
        if (Input.GetKey(KeyCode.DownArrow))　   
        {
            transform.Translate(0, -0.05f, 0);　　
        }
    }
  
}

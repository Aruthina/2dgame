﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class GameDirector : MonoBehaviour
{
    GameObject hpgauge;
    int hpGauge = 10;
    public int healHP;

    // Start is called before the first frame update
    void Start()
    {
        hpgauge = GameObject.Find("hpGauge");
    }

    public void DecreaseHp()
    {
        hpgauge.GetComponent<Image>().fillAmount -= 0.1f;
        GetComponent<AudioSource>().Play();
        hpGauge -=1;
        if(hpGauge == 0)
        {
            SceneManager.LoadScene("GameOver");　　//ゲームオーバーシーンに
        }
    }
    public void IncreaseHp()　　　　　//HP回復処理
    {
        hpgauge.GetComponent<Image>().fillAmount += 0.1f;
        hpGauge += 1;
    }
}

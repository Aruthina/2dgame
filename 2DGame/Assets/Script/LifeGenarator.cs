﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LifeGenarator : MonoBehaviour
{
    public GameObject hpHealthPrefab;
    float span = 15.0f;
    float detal = 0;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        this.detal += Time.deltaTime;
        if (this.detal > this.span)
        {
            this.detal = 0;
            GameObject go = Instantiate(hpHealthPrefab) as GameObject;
            int py = Random.Range(-3, 5);
            go.transform.position = new Vector3(50, py, 0);
        }
    }
}

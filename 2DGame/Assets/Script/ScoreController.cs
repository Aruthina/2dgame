﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class ScoreController : MonoBehaviour
{
    public static int score = 0;
    GameObject scoreText;

    GameObject highscoreText;
    private int highscore = 0;
    private string key = "HIGH SCORE";

    public void Addscore()
    {
        score += 10;　　//score+10ずつ
        GetComponent<AudioSource>().Play();　　　//音追加
    }

    public void Addscore2()
    {
        score += 100;
        GetComponent<AudioSource>().Play();
    }
    // Start is called before the first frame update
    void Start()
    {
        score = 0;
        this.scoreText = GameObject.Find("score");
        this.highscoreText = GameObject.Find("Highscore");
        highscore = PlayerPrefs.GetInt(key, 0); //保存したハイスコアをキーで呼び出し取得し保存されていれば0になる
        
    }
  
    // Update is called once per frame
    public void Update()
    {
        scoreText.GetComponent<Text>().
            text = "Score:" + score.ToString("D4");

        highscoreText.GetComponent<Text>().
            text = "HighScore:" + highscore.ToString();
        
        if(score > highscore)
        {
            highscore = score; //ハイスコア更新
            PlayerPrefs.SetInt(key, highscore);  //ハイスコア保存
            
        }
    }
}
